FROM ubuntu:20.04

COPY . ./daq

RUN export DEBIAN_FRONTEND=noninteractive; \
    apt-get -qq update > /dev/null; \
    apt-get -qq upgrade > /dev/null; \
    apt-get -qq install -y libssl-dev \
                           python3 \
                           python3-pip \
                           git \
                           libudev-dev \
                           libusb-1.0.0-dev \
                           usbutils > /dev/null; \
    # Clone git repos & install exodriver
    git clone https://github.com/labjack/exodriver.git ./exodriver; \
    git clone https://github.com/labjack/LabJackPython.git ./LabJackPython; \
    cd ./exodriver/liblabjackusb && make > /dev/null && make install > /dev/null; cd /; \
    # Install python environment
    python3 -m pip install -q -e LabJackPython; \
    python3 -m pip install -q -e daq; \
    # Clean up to reduce size of image
    apt-get -qq remove -y git libssl-dev build-essential > /dev/null; \
    apt-get -qq autoremove -y > /dev/null;
