from setuptools import setup, find_packages

setup(
    name='daq',
    version='0.0',
    description='',
    author='James Logan',
    author_email='jlogan03@gmail.com',
    packages=find_packages(),
    install_requires=[
        "sphinx",
        "numpy",
        "scipy",
        "matplotlib",
        "networkx",
        "pandas",
        "bokeh",
        "psutil"
    ],  # external packages as dependencies
)
