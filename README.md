# DAQ

A library for abstracting away the details of data acquisition.

API and implementation details: https://daq.readthedocs.io/en/latest/

The structure of the library is based on these conceptual cuts, which are chosen
to separate behaviors by relative timescale and direction of flow of information.

    There are two common applications of DAQs that are functionally distinct
    1.  Control: Low-rate non-buffering read/command
    2.  Stream: High-rate buffering read
    
    ...the DAQ's fundamental operations are
    1.  Configure
    2.  Read
    3.  Command
    4.  Stream
    
    ...and the data acquisition process represents one slow and one fast task
    1.  Preparation: Anything not real-time responsive
    2.  Operation: Anything real-time responsive

This is much leaner than the feature list of most hardware on the market!

Here's an example run in control mode at 300Hz with 2 output channels and
16 input channels on a LabJack U3-HV.
```
# Imports & plotting omitted
try:
    device = u3.U3()                                  # User provides third-party device drivers
    interface = LabJackU3HVInterface(device=device,   
                       config_path=daq_json_path)     # User provides one-time configuration
    operator = Operator(interface=interface, config_dir=operator_config_dir)
    dataframe = operator.run()                        # Manipulate hardware and return data  
finally:
    interface.close()
```
![analog read](/ci/docs/source/readme_plot_0.png)
![timing margin](/ci/docs/source/readme_plot_1.png)

In this particular application, three distinct blocks of commands are run (first, second, final)
and timing is carried across the transitions with no discernible loss in accuracy. The connections
between command blocks are stored as a graph structure and traversed according to preset rules.

![block local time](/ci/docs/source/readme_plot_2.png)
![block_graph](/ci/docs/source/readme_plot_3.png)

## Mission Statement & Requirements

    To produce a data acquisition framework in which
    1. Processes are independent of device specifics.
    2. The user need only know their intent and their device capabilities.
    3. Capability is limited by hardware, not software.
    4. The process is convenient to consume either programatically or manually.

This leads to a handful of more specific requirements
1.  The device should contribute capabilities, *not structure*.
2.  All device-specific actions should be confined to one-time configuration at device power-on, using a single file of a standard format.
3.  All device-specific functionality should be mapped onto the four fundamental operations.
4.  All devices should have a common interface after one-time configuration.
5.  All computation not dependent on observations should be performed during preparation, *not during operation*.
6.  The operation stage of the data acquisition process should be a function call that manipulates hardware and returns data.
7.  The operation stage should be formulated *as a graph structure* to facilitate visualization and analysis.

## Control Mode vs. Stream Mode

In control mode, time is of the essence. Response to each set of observations 
must be swift and reliable. The need to communicate to the device during operation
significantly limits samplerate (typically by a factor of 1e2-1e3).

In stream mode, detailed observations are required, but immediate responses are not.
Buffering allows significantly increased data rate by eliminating communication
with the device during operation, but is typically not viable inside a control loop.

For example, a PID controller could be implemented easily in control mode, but likely 
could not accommodate the buffering delay in stream mode. Meanwhile, it is typically hard
to observe phenomena such as mechanical vibrations at a rate compatible with control.

