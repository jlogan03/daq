.. daq documentation master file, created by
   sphinx-quickstart on Fri May  3 23:22:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DAQ
===

Gitlab project: https://gitlab.com/jlogan03/daq

.. toctree::
   :maxdepth: 2

   autodoc
