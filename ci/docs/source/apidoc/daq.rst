daq package
===========

.. automodule:: daq
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   daq.hardware

Submodules
----------

.. toctree::

   daq.operator
   daq.visuals
