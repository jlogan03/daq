#!/bin/sh

BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [[ "$BRANCH" == "master" ]]
then
  { \
    docker push registry.gitlab.com/jlogan03/daq; \
  } || { echo 1; }
fi
