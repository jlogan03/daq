#!/bin/sh

run_docker() { \
    # Run the daq docker image with configured drivers
    sudo docker login registry.gitlab.com;
    sudo docker pull registry.gitlab.com/jlogan03/daq;
    sudo docker run -it \
    --privileged -v /dev/bus/usb:/dev/bus/usb \
    registry.gitlab.com/jlogan03/daq \
    bash; \
}

run_docker
